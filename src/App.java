public class App {
    public static void main(String[] args) throws Exception {
        AvionCarga objAvionCarga = new AvionCarga("Gris", 150.5);
        objAvionCarga.cargar();
        System.out.println(objAvionCarga.despegar());
        objAvionCarga.aterrizar();
        objAvionCarga.descargar();
        System.out.println("********avion pasajeros******");
        AvionPasajeros objAvionPasajeros = new AvionPasajeros("Plateado",150.5,160);
        objAvionPasajeros.despegar();
        objAvionPasajeros.servir();
        objAvionPasajeros.aterrizar();
        System.out.println("********avion militar******");
        AvionMilitar objAvionMilitar = new AvionMilitar("Verde",150.5,30);
        objAvionMilitar.despegar();
        
    }
}
