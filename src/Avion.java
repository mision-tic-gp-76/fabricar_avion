public class Avion{
    /**********************
     * Atributos
     ********************/

    private String color;
    private double tamanio;

    /********************
      * Constructores
      ********************/
    public Avion(String color, double tamanio){
        this.color = color;
        this.tamanio = tamanio;

    }
    //public Avion(){
    //
    //}

    /**********************
     * Metodos
     * (Acciones de la clase)
     ************************/
    public void aterrizar(){
        System.out.println("Aterrizando......");
    }

    public boolean despegar(){
        System.out.println("Despegando.....");
        return true;
    }

    public void frenar(){
        System.out.println("Frenando........");
    }
}