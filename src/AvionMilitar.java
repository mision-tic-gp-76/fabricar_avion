public class AvionMilitar extends Avion {
    /**************
     * Atributos
     **************/
    private int misiles;

    /*********************
     * Constructor
     *********************/
    public AvionMilitar(String color, double tamanio, int misiles){
        super(color, tamanio);
        this.misiles = misiles;
    }

    private void disparar(){
        System.out.println("Disparando misil.......");
    }
    
}
