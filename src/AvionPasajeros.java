public class AvionPasajeros extends Avion {
    /**************
     * Atributos
     **************/
    private int pasajeros;
    /*********************
     * Constructor
     *********************/
    public AvionPasajeros(String color, double tamanio, int pasajeros){
        super(color, tamanio);
        this.pasajeros = pasajeros;
    }

    /*****************
     * Modificador
     *****************/
    public void setPasajeros(int pasajeros){
        this.pasajeros = pasajeros;
    }

    /*******************
     * Métodos
     *******************/
    public void servir(){
        System.out.println("Sirviendo a los pasajeros.....");
    }
}
